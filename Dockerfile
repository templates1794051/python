FROM python:3.11.6-slim-bookworm

ENV PYTHONUNBUFFERED=1 \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.7.0 \
  PYTHONPATH=/app

# System deps:
RUN pip install "poetry==$POETRY_VERSION"

# install dependecy packages
RUN apt-get update && \
    # package 👇 for test
    apt-get install ssh -y && \
    apt-get clean autoclean && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /app

# Project initialization:
COPY pyproject.toml poetry.lock ./
RUN poetry config virtualenvs.create false && \
    poetry config installer.max-workers 10 && \
    poetry install --no-interaction --no-ansi

# Creating folders, and files for a project:
COPY . .

CMD ["poetry", "run", "uvicorn", "app.main:app", "--host", "0.0.0.0","--port", "5000"]

