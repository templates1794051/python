# Python Template

## This is python application architecture template

Just clone it for your new project and implement your business logic

```
app
|
| -- \tables - схемы
|
| -- \repository - CRUDs (use only DB layer)
|
| -- \models - pydantic DTO
|
| -- \routes - API endpoints (user only service layer)
|
| -- \core - reusable logic
|
migrations
|
tests
|
_
```

The main feature of this architecture is the most flexibility.
You can remove any unused layer, and project will run.

This is the best case for start-up or hackathon, where you need to test whether clients like that or not.

It is also a good base for extend and customize architecture.

- [ ] Minimum nesting of modules
- [ ] Tests
- [ ] CI\CD
- [ ] ORM vs SQL - You can quickly swap on one of them

## Repository

There is **BaseRepository** that implements the most used CRUD operations with async ORM.
That allows to work with many entities more quickly.

```python
class UsersRepository(BaseRepository):
    table = users_table
    model = UserInDB
```

- `table` - sqlalchemy.Table
- `model` - pydantic model that stores in database

> Every repository operation must return database entity that will cast to `model`

## Routers

There is **RouterTemplate** that implements the most used CRUD endpoints.

- [ ] `retrieve/{id}`
- [ ] `update/{id}`
- [ ] `delete/{id}`
- [ ] `get_all/?limit,offset,sort`
- [ ] `create/`

<img src="img/crud_swagger.png" alt="drawing" width="700"/>


That allows to create many base endpoints and customize it.

```python
trails_config = RouteConfig(
    create_model=CreateTrailData,
    update_model=UpdateTrailData,
    response_model=TrailInDB,
    repository=TrailsRepository,
)
trails = TemplateRouter(
    config=trails_config,
    prefix="/trails",
    tags=["Trail"]
)

trails.generate()
```

For each TemplateRouter you must define

- config
    - `create_model` - incoming pydantic type for create request
    - `update_model` - incoming pydantic type for update request
    - `response_model` - response pydantic type for requests
    - `repository` - Any `BaseRepository` children
- prefix
- tags

`trails.generate()` - generate API endpoints. Should invoke at the end.

## Customize

Create custom CREATE endpoint callback
```python
@trails.create_handler
async def create_trail(
        cmd: CreateTrailData,
        user_id: uuid.UUID = Depends(get_token),
        repo=Depends(TrailsRepository),
):
    return await repo.create(
        **cmd.model_dump(),
        user_id=user_id,
    )
```

This CRUD generator support api callbacks overriding.
May be cases there you have to implement explicit business logic with multi state/repository steps.

There are decorators:

- `retrieve_handler(fn)`
- `get_all_handler(fn)`
- `create_handler(fn)`
- `update_handler(fn)`
- `delete_handler(fn)`

## Pre-commit

### Windows

1. Global install dev dependencies #TODO: may not need install global, only into poetry venv
```cmd
pip install --global black isort pycln pre-commit
```
2. pre-commit (in project directory)
```cmd
pre-commit install
```

**WARNING**: if you works with PyCharm, then the latter will require pressing the commit button twice

### Linux 🤡

Wait for luck linux owner write this section 
