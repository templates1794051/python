from uuid import UUID


def get_token():
    """JWT Auth. Return User model/user_id"""
    return UUID("db1a610e-c8e6-405c-9f6c-4cae9c681730")
