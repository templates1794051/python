import json
import os
from functools import lru_cache

import dotenv
from pydantic import SecretStr
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    app_host: str = "0.0.0.0"
    app_port: int = 5000

    postgres_user: str = "postgres"
    postgres_password: SecretStr = SecretStr("postgres")
    postgres_db: str = "postgres"
    postgres_host: str = "localhost"
    postgres_port: str = "5432"

    @property
    def postgres_url(self):
        return (
            f"postgresql+psycopg2://"
            f"{self.postgres_user}:{self.postgres_password.get_secret_value()}"
            f"@{self.postgres_host}/{self.postgres_db}"
        )

    @property
    def async_postgres_url(self):
        return (
            f"postgresql+asyncpg://"
            f"{self.postgres_user}:{self.postgres_password.get_secret_value()}"
            f"@{self.postgres_host}/{self.postgres_db}"
        )


@lru_cache
def get_settings() -> Settings:
    dotenv.load_dotenv()
    print("------ GET SETTINGS ------")
    settings = Settings()
    print(json.dumps(settings.model_dump(), default=str, indent=4))
    print("------ END SETTINGS ------")
    return settings
