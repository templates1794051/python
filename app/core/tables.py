import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID

metadata = sa.MetaData()


def get_uuid_id_field():
    return sa.Column(
        "id",
        UUID,
        primary_key=True,
        nullable=False,
        server_default=sa.text("gen_random_uuid()"),
    )


def get_created_at_field():
    return sa.Column(
        "created_at",
        sa.TIMESTAMP,
        nullable=False,
        server_default=sa.func.now(),
    )


def get_updated_at_field():
    return sa.Column(
        "updated_at",
        sa.TIMESTAMP,
        nullable=True,
        onupdate=sa.func.now(),
    )
