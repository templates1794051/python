from fastapi import FastAPI

from app.core import get_settings
from app.routes.trails import trails
from app.routes.users import user

app = FastAPI()

app.include_router(user.router)
app.include_router(trails.router)

if __name__ == "__main__":
    import asyncio
    import uvicorn

    settings = get_settings()

    serv_config = uvicorn.Config(
        app=app,
        host=settings.app_host,
        port=settings.app_port,
        timeout_graceful_shutdown=5,
    )
    server = uvicorn.Server(serv_config)

    asyncio.run(server.serve())
