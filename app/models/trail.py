import datetime
import decimal
from typing import Optional
from uuid import UUID

from pydantic import BaseModel, Field


class TrailInDB(BaseModel):
    id: UUID
    created_at: datetime.datetime
    created_at: Optional[datetime.datetime]
    polyline: list[list[decimal.Decimal]]
    title: str
    description: str
    user_id: UUID


class CreateTrailData(BaseModel):
    """Get on router"""

    polyline: list[list[decimal.Decimal]] = Field(examples=[[[1.3, 5.24]]])
    title: str = Field(examples=["forest"])
    description: str = Field(examples=["green place"])


class CreateTrailCommand(CreateTrailData):
    """Pass to repository"""

    user_id: UUID


class UpdateTrailData(BaseModel):
    polyline: Optional[list[list[decimal.Decimal]]] = Field(
        examples=[[[1.3, 5.24]]], default=None
    )
    title: Optional[str] = Field(examples=["forest"], default=None)
    description: Optional[str] = Field(examples=["green place"], default=None)
