import datetime
from typing import Optional
from uuid import UUID

from pydantic import BaseModel, Field


class UserInDB(BaseModel):
    id: UUID
    created_at: datetime.datetime
    updated_at: Optional[datetime.datetime]
    login: str = Field(examples=["admin"])
    password_hash: str = Field(examples=["12345"])
    age: int = Field(examples=[21])
    phone: str = Field(examples=["88005553535"])
    logo_url: Optional[str] = Field(examples=["https://domain.ru/img/1425443"])


class CreateUserData(BaseModel):
    login: str = Field(examples=["admin"])
    password_hash: str = Field(examples=["12345"])
    age: int = Field(examples=[21])
    phone: str = Field(examples=["88005553535"])
    logo_url: Optional[str] = Field(examples=["https://domain.ru/img/1425443"])


class UpdateUserData(BaseModel):
    login: Optional[str] = Field(examples=["user_1234"])
    password_hash: Optional[str] = Field(examples=["6543"])
    age: Optional[int] = Field(examples=[22])
    phone: Optional[str] = Field(examples=["89557388181"])
