from app.core.repository import BaseRepository
from app.models.trail import TrailInDB
from app.models.user import UserInDB
from app.tables.trails import trails_table
from app.tables.users import users_table


class TrailsRepository(BaseRepository):
    table = trails_table
    model = TrailInDB


class UsersRepository(BaseRepository):
    table = users_table
    model = UserInDB
