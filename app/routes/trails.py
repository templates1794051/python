import uuid

from fastapi import Depends

from app.core.auth import get_token
from app.core.routes import TemplateRouter
from app.models.config import RouteConfig
from app.models.trail import CreateTrailData, TrailInDB, UpdateTrailData
from app.repository.store import TrailsRepository

trails_config = RouteConfig(
    create_model=CreateTrailData,
    update_model=UpdateTrailData,
    response_model=TrailInDB,
    repository=TrailsRepository,
)

trails = TemplateRouter(config=trails_config, prefix="/trails", tags=["Trail"])


@trails.create_handler
async def create_trail(
    cmd: CreateTrailData,
    user_id: uuid.UUID = Depends(get_token),
):
    repo = TrailsRepository()
    return await repo.create(
        **cmd.model_dump(),
        user_id=user_id,
    )


trails.generate()
