from app.core.routes import TemplateRouter
from app.models.config import RouteConfig
from app.models.user import CreateUserData, UpdateUserData, UserInDB
from app.repository.store import UsersRepository

user_config = RouteConfig(
    create_model=CreateUserData,
    update_model=UpdateUserData,
    response_model=UserInDB,
    repository=UsersRepository,
)

user = TemplateRouter(config=user_config, prefix="/users", tags=["Users"])

user.generate()
