import sqlalchemy as sa

from app.core.tables import (
    get_created_at_field,
    get_updated_at_field,
    get_uuid_id_field,
    metadata,
)
from app.tables.users import users_table

trails_table = sa.Table(
    "trails",
    metadata,
    get_uuid_id_field(),
    get_created_at_field(),
    get_updated_at_field(),
    sa.Column(
        "polyline",
        sa.ARRAY(sa.Float),
        nullable=False,
    ),
    sa.Column(
        "title",
        sa.String,
        nullable=False,
    ),
    sa.Column(
        "description",
        sa.String,
        nullable=False,
    ),
    sa.Column(
        "user_id",
        sa.ForeignKey(users_table.c.id),
        nullable=False,
    ),
)
