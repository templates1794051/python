import sqlalchemy as sa

from app.core.tables import (
    get_created_at_field,
    get_updated_at_field,
    get_uuid_id_field,
    metadata,
)

users_table = sa.Table(
    "users",
    metadata,
    get_uuid_id_field(),
    get_created_at_field(),
    get_updated_at_field(),
    sa.Column(
        "login",
        sa.VARCHAR(255),
        nullable=False,
    ),
    sa.Column(
        "password_hash",
        sa.String,
        nullable=False,
    ),
    sa.Column(
        "age",
        sa.Integer,
        nullable=True,
    ),
    sa.Column(
        "phone",
        sa.String,
        nullable=False,
    ),
    sa.Column(
        "logo_url",
        sa.String,
        nullable=True,
    ),
)
